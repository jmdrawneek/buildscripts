#!/usr/bin/env bash

# Install virtual box and vagrant
sudo apt-get install virtualbox -y

# Need to figure out how to get the latest vagrant at this one is too old
sudo apt-get install vagrant -y

sudo apt-get install ansible -y

apt-get install nfs-common -y

# Fix kernal stuff for virtual box
sudo apt-get install virtualbox-dkms -y

#make vb work!
sudo dpkg-reconfigure virtualbox-dkms
sudo dpkg-reconfigure virtualbox




# Install Jenkins
wget -q -O - https://jenkins-ci.org/debian/jenkins-ci.org.key | sudo apt-key add -
sudo sh -c 'echo deb http://pkg.jenkins-ci.org/debian binary/ > /etc/apt/sources.list.d/jenkins.list'
sudo apt-get update
sudo apt-get install jenkins -y

sudo apt-get install git -y