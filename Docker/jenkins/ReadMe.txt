# Add plugins to the plugins.txt file to have them added to the jenkins image if required

# Make sure you're in the folder with the Dockerfile and run this
docker build -t curve-ci/jenkins:v1 .

# After the image is made you can run the below to start the Jenkins server on port 8080
docker run -p 8080:8080 -p 50000:50000 -v ~/jenkins-files:/var/jenkins_home jmdrawneek/jenkins:v1

