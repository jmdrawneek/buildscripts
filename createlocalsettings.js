/**
 * Created by JimCurve on 07/01/2016.
 */
var fs = require('fs');

exec("cd /var/lib/jenkins/workspace/" + project + "/docroot/sites/default/", shellcmd);

var localSettings = "<?php\
        $databases['default']['default'] = array (\
                'database' => '" + project + "',\
        'username' => 'root',\
        'password' => 'root',\
        'host' => '127.0.0.1',\
        'port' => 3306,\
        'driver' => 'mysql',\
        'prefix' => '',\
);\
// Disable caching for local development\
unset($conf['cache_backends']);\
unset($conf['cache_backends']);\
unset($conf['cache_default_class']);\
unset($conf['cache_class_cache_form']);\
unset($conf['cache_class_cache_page']);";

fs.writeFile("/tmp/test", localSettings, function(err) {
    if(err) {
        return console.log(err);
    }

    console.log("The file was saved!");
});


drush si minimal --db-url=mysql://root:root@localhost:3306/lvhppr --db-su=root --db-su-pw=root --site-name="lvhppr"


